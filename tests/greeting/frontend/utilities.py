import tests.greeting.utilities


def get_format():
    raise NotImplementedError


def greet(name):
    return tests.greeting.utilities.get_format() % name
