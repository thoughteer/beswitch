from tests.greeting.backends.formal.utilities import *


__get_format = get_format
def get_format():
    return __get_format() + " How do you do?"
