from tests.greeting.backends.polite.utilities import *


def greet(name):
    polite_greeting = get_format() % name
    saucy_response = "Screw you, %s!" % name
    return "You expect me to say \"" + polite_greeting + "\"? " + saucy_response
