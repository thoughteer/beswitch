import tests.fancynaming
import tests.settings


def test_fancynaming():
    test_name = tests.settings.TEST_NAME
    expected_greeting = tests.settings.EXPECTED_GREETING
    assert tests.fancynaming.greet_fancy(test_name) == expected_greeting
