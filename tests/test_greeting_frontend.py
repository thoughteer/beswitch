import pytest
import tests.greeting.frontend.utilities
import tests.settings


def test_get_format():
    with pytest.raises(NotImplementedError):
        tests.greeting.frontend.utilities.get_format()


def test_greet():
    greeting = tests.greeting.frontend.utilities.greet(tests.settings.TEST_NAME)
    assert greeting == tests.settings.EXPECTED_POLITE_GREETING
