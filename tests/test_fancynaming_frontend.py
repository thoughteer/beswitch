import pytest
import tests.fancynaming.frontend
import tests.settings


def test_decorate():
    with pytest.raises(NotImplementedError):
        tests.fancynaming.frontend.decorate(tests.settings.TEST_NAME)


def test_greet_fancy():
    greeting = tests.fancynaming.frontend.greet_fancy(tests.settings.TEST_NAME)
    assert greeting == tests.settings.EXPECTED_GREETING
