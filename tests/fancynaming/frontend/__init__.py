import tests.fancynaming
import tests.greeting


def decorate(name):
    raise NotImplementedError


def greet_fancy(name):
    return tests.greeting.greet(tests.fancynaming.decorate(name))


__all__ = ["decorate", "greet_fancy"]
