# Beswitch

Python 3.x package for easy backend switching.

---

## License

**Beswitch** is released under the MIT license.

---

## Installation

Installing **Beswitch** is as simple as
```console
$ pip install beswitch
```
if you use [pip](https://pypi.python.org/pypi/pip).
Otherwise, you can manually clone this repository and run
```console
$ python setup.py install
```

---

## Usage

**Beswitch** helps to maintain and switch among several interchangeable
implementations of your Python 3.x package.
The implementations are further referred to as *backends*, and their common
interface exposed to the user is called *frontend*.

**Beswitch** requires you to structure your package as follows:

```text
yourpackage             # Root of the package.
|-- __init__.py         # Selects one of the backends.
|-- frontend            # Interface (basically abstract) part
|   |-- __init__.py     # can have arbitrary structure.
|   `-- ...
|-- backends
|   |-- __init__.py     # Should be empty.
|   |-- firstbackend/   # Implementation of the interface
|   |   |-- __init__.py # partially mimics the frontend's structure.
|   |   `-- ...
|   |-- secondbackend/  # Another implementation.
|   |   |-- __init__.py
|   |   `-- ...
|   `-- ...
`-- ...                 # You better stop at this point.
```

Each backend should be derived from either the frontend or another backend by
calling `beswitch.derive`.
For instance, `secondbackend` in the example package above could be derived from
either the frontend with
```python
# in `yourpackage/backends/secondbackend/__init__.py`
import beswitch
beswitch.derive()
```
or from `firstbackend` with
```python
# in `yourpackage/backends/secondbackend/__init__.py`
import beswitch
beswitch.derive("firstbackend")
```

Now, your package can select an implementation to be used by calling
`beswitch.select`, e.g.
```python
# in `yourpackage/__init__.py`
import beswitch
beswitch.select("secondbackend")
```

**Beswitch** will take care of proper import redirection and load all necessary
modules.

---

## Remarks

#### Public attributes of the frontend

The frontend should explicitly expose its public attributes via `__all__` if
any.

#### Intrapackage references

In order to call a package function `foo` in the frontend, do
```python
import yourpackage.module
yourpackage.module.foo()
```
instead of simple
```python
foo()
```
because the former will call the actual implementation of `foo`; the
latter is most likely to call its unimplemented prototype.
This rule goes for all intrapackage references (modules, classes, etc.).

#### Backend inheritance

Each module of a backend should start with
```python
from yourpackage.frontend.module import *
```
if the backend was derived from the frontend, and
```python
from yourpackage.backends.parentbackend.module import *
```
if it was derived from some `parentbackend`.
This helps to ensure that the backends conform to the common interface.

---

## Testing

You can test **Beswitch** using [pytest](http://pytest.org):
```console
$ py.test
```